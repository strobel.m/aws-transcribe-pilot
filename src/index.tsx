import Amplify from 'aws-amplify';
import awsExports from './aws-exports';
Amplify.configure(awsExports);

import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { reportReducer } from './state/reducers';

import App from './App';

const store = createStore(
    reportReducer,
    // @ts-ignore-next-line
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('report-editor')
);
