import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { ReportState, DispatchReport } from '../state/types';
import { Transcribe } from '../AwsTranscribe';

type TranscribeWrapperProps = { transcribe: Transcribe };
export default function TranscribeWrapper({ transcribe }: TranscribeWrapperProps) {
    const isDictating = useSelector<ReportState, boolean>(state => state.dictating);
    if (isDictating) {
        transcribe.start();
    } else {
        transcribe.stop();
    }

    const dispatch: DispatchReport = useDispatch();
    useKeyPress('F11', dispatch);

    return null;
}

function useKeyPress(targetKey: string, dispatch: DispatchReport) {
    let keyPressed = false;
    let lastDown = Date.now();
    const isDictating = useSelector<ReportState, boolean>(state => state.dictating);

    function downHandler({ key }: KeyboardEvent) {
        if (key === targetKey) {
            keyPressed = true;
            lastDown = Date.now();
            if (!isDictating) {
                dispatch({ type: 'START_DICTATING' });
            }
            // TODO: workaround
            setTimeout(function () {
                const dt = Date.now() - lastDown;
                if (dt > 450) {
                    dispatch({ type: 'STOP_DICTATING' });
                    // transcribe.stop();
                }
            }, 500);
        }
    }
    const upHandler = ({ key }: KeyboardEvent) => {
        if (key === targetKey) {
            keyPressed = false;
            dispatch({ type: 'STOP_DICTATING' });
        }
    };

    useEffect(() => {
        window.addEventListener('keydown', downHandler);
        window.addEventListener('keyup', upHandler);
        return () => {
            window.removeEventListener('keydown', downHandler);
            window.removeEventListener('keyup', upHandler);
        };
    }, []);

    return keyPressed;
}
