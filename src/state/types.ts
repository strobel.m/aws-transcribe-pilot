export const OPEN_REPORT = 'OPEN_REPORT';
export const CLOSE_REPORT = 'CLOSE_REPORT';
export const START_DICTATING = 'START_DICTATING';
export const STOP_DICTATING = 'STOP_DICTATING';
export const TOGGLE_DICTATING = 'TOGGLE_DICTATING';

interface OpenReportAction {
    type: typeof OPEN_REPORT;
}

interface CloseReportAction {
    type: typeof CLOSE_REPORT;
}

interface StartDictatingAction {
    type: typeof START_DICTATING;
}

interface StopDictatingAction {
    type: typeof STOP_DICTATING;
}

interface ToggleDictatingAction {
    type: typeof TOGGLE_DICTATING;
}

export type ReportActionTypes =
    | OpenReportAction
    | CloseReportAction
    | StartDictatingAction
    | StopDictatingAction
    | ToggleDictatingAction;

export interface ReportState {
    reportOpen: boolean;
    dictating: boolean;
}

export type DispatchReport = (arg: ReportActionTypes) => ReportActionTypes;
