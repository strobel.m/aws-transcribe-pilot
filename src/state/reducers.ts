import {
    OPEN_REPORT,
    CLOSE_REPORT,
    START_DICTATING,
    STOP_DICTATING,
    TOGGLE_DICTATING,
    ReportActionTypes,
    ReportState,
} from './types';

export const initialState: ReportState = {
    reportOpen: true,
    dictating: false,
};

export function reportReducer(state = initialState, action: ReportActionTypes): ReportState {
    switch (action.type) {
        case OPEN_REPORT:
            return {
                ...state,
                reportOpen: true,
                dictating: false,
            };
        case CLOSE_REPORT:
            return {
                ...state,
                reportOpen: false,
                dictating: false,
            };
        case START_DICTATING:
            return {
                ...state,
                dictating: true,
            };
        case STOP_DICTATING:
            return {
                ...state,
                dictating: false,
            };
        case TOGGLE_DICTATING:
            return {
                ...state,
                dictating: !state.dictating,
            };
        default:
            return state;
    }
}
