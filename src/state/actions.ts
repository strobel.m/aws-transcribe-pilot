import { OPEN_REPORT, CLOSE_REPORT, ReportActionTypes } from './types';

export function openReport(): ReportActionTypes {
    return {
        type: OPEN_REPORT,
    };
}

export function closeReport(): ReportActionTypes {
    return {
        type: CLOSE_REPORT,
    };
}
