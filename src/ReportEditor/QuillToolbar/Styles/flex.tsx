import styled, { css } from 'styled-components';

export const FlexRowBaseline = css`
    display: flex;
    align-items: baseline;
`;

export const CentredBoxStyle = css`
    ${FlexRowBaseline}
    justify-content: center;
`;

export const CentredBox = styled.span`
    ${CentredBoxStyle}
`;

export const FlexColumn = css`
    display: flex;
    flex-direction: column;
`;

export const ColumnDiv = styled.div`
    ${FlexColumn}
`;

export const ColumnUl = styled.ul`
    ${FlexColumn}
    padding-left: 0;
    margin: 0;
`;

export const ColumnLi = styled.li`
    ${FlexColumn}
`;

export const CenteredRowSpan = styled.span`
    ${FlexRowBaseline}
`;

export const CenteredRowDiv = styled.div`
    ${FlexRowBaseline}
`;
