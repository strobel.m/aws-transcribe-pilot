import styled, { css } from 'styled-components';

export const IconFonts = css`
    font-family: 'sr-icons' !important;
    font-style: normal;
    font-weight: normal;
    font-variant: normal;
`;

export function IconStyle(icon: Icon) {
    return css`
        ${IconFonts}
        content: "${icon}";
    `;
}

export const StyledIcon = styled.i<{ icon: Icon }>`
    &::before {
        ${props => IconStyle(props.icon)}
    }
`;

export const enum Icon {
    ArrowUp = '\ue917',
    ArrowDown = '\ue909',
    ArrowLeft = '\ue90f',
    ArrowRight = '\ue910',
    Bold = '\ue900',
    BulletList = '\ue903',
    Checkmark = '\ue908',
    Dot = '\ue90a',
    Empty = '',
    Image = '\ue905',
    Italic = '\ue901',
    Moon = '\ue907',
    OrderedList = '\ue904',
    Sun = '\ue906',
    Underline = '\ue902',
    Mic = '\ue90c',
    Dots = '\ue90b',
    Search = '\ue90d',
    Profile = '\ue90e',
    Close = '\ue911',
    Warning = '\ue912',
    Plus = '\ue913',
    Delete = '\ue914',
    Edit = '\ue915',
    Home = '\ue916',
    Eye = '\ue918',
    EyeOpened = '\ue919',
    Reset = '\ue91a',
    Hamburger = '\ue91b',
    SReportingLogo = '\ue91c',
    Lock = '\ue91d',
    AutosaveIdle = '\ue921',
    AutosaveSaving = '\ue920',
    AutosaveError = '\ue91f',
    InputEmpty = '\ue922',
    InputImage = '\ue923',
    InputTable = '\ue924',
    SelectionEmpty = '\ue925',
    SelectionFull = '\ue926',
    SelectionHalf = '\ue927',
    Info = '\ue928',
    Caret = '\ue929',
}
