import styled from 'styled-components';
import convert from 'color-convert';

import { CenteredRowSpan, CenteredRowDiv } from './Styles/flex';
import { IconButton } from './ReusableComponents/IconButton';

const enum Constants {
    ToolbarHeight = 48,
    SpaceS = 8,
    SpaceM = 16,
    ButtonGroupLeftPaddingPx = 40,
    ButtonBoxInsetPx = 4,
    TextGridPx = 880,
}

export const Toolbar = styled(CenteredRowDiv)`
    border-bottom: 1px solid ${props => props.theme.colors.bg.tertiary};
    background-color: ${props => props.theme.colors.bg.secondary};
    padding: ${Constants.SpaceS}px ${Constants.SpaceM}px;
`;

export const SideToolbar = styled(CenteredRowDiv)`
    flex: 1;
`;

export const GridToolbar = styled(CenteredRowDiv)`
    flex: ${Constants.TextGridPx}px 0 1;
    justify-content: space-between;
`;

export const ButtonGroup = styled(CenteredRowSpan)`
    padding-left: ${Constants.ButtonGroupLeftPaddingPx}px;

    :first-child {
        padding-left: 0;
    }

    .ql-active {
        color: ${props => props.theme.colors.semantic.cta} !important;
        background-color: ${props => props.theme.colors.semantic.cta};
        background: rgba(
            ${props => convert.hex.rgb(props.theme.colors.semantic.cta).toString()},
            0.1
        );
    }
`;

export const Button = styled(IconButton)`
    margin-right: ${Constants.ButtonBoxInsetPx}px;

    svg {
        display: none;
    }
`;
