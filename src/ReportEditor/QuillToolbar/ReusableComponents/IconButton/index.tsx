import styled from 'styled-components';

import { CentredBoxStyle } from '../../Styles/flex';
import { Icon, IconStyle } from '../../Styles/icons';

const enum Constants {
    ButtonBoxSideLengthPx = 32,
    ButtonBoxBorderRadiusPx = 3,
    IconSize = 19,
}

export const AbstractIconButton = styled.button`
    ${CentredBoxStyle}
    cursor: pointer;

    color: ${props => props.theme.colors.base.secondary};
    background-color: ${props => props.theme.colors.bg.secondary};

    :hover {
        background-color: ${props => props.theme.colors.bg.tertiary};
    }

    border: none;
    border-radius: ${Constants.ButtonBoxBorderRadiusPx}px;

    width: ${Constants.ButtonBoxSideLengthPx}px;
    height: ${Constants.ButtonBoxSideLengthPx}px;

    font-size: ${Constants.IconSize}px;
    line-height: ${Constants.ButtonBoxSideLengthPx}px;
`;

export const IconButton = styled(AbstractIconButton)<{ icon: Icon }>`
    &::before {
        ${props => IconStyle(props.icon)}
    }
`;
