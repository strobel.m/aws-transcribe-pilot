import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import * as Styles from './styles';
import { Icon } from './Styles/icons';

import { ReportState, ReportActionTypes } from '../../../../aws-transcribe-pilot/src/state/types';

type DispatchReport = (arg: ReportActionTypes) => ReportActionTypes;

interface QuillToolbarProps {
    readonly toolbarRef: React.RefObject<HTMLDivElement>;
}

export function QuillToolbar(props: React.PropsWithChildren<QuillToolbarProps>) {
    const dispatch: DispatchReport = useDispatch();
    const [micEnabled, setMicEnabled] = useState(false);
    const isDictating = useSelector<ReportState, boolean>(state => state.dictating);

    function toggleMic() {
        if (!micEnabled) {
            dispatch({ type: 'START_DICTATING' });
        } else {
            dispatch({ type: 'STOP_DICTATING' });
        }
        setMicEnabled(!micEnabled);
    }

    return (
        <Styles.Toolbar ref={props.toolbarRef}>
            <Styles.SideToolbar>
                <Styles.ButtonGroup>{/* Expand/Collapse sidebar button */}</Styles.ButtonGroup>
            </Styles.SideToolbar>
            <Styles.GridToolbar>
                <Styles.ButtonGroup>
                    <Styles.ButtonGroup>
                        <Styles.Button
                            className={isDictating ? 'ql-mic ql-active' : 'ql-mic'}
                            icon={Icon.Mic}
                            onClick={toggleMic}
                        />
                    </Styles.ButtonGroup>

                    <Styles.ButtonGroup>
                        <Styles.Button className="ql-bold" icon={Icon.Bold} />
                        <Styles.Button className="ql-italic" icon={Icon.Italic} />
                        <Styles.Button className="ql-underline" icon={Icon.Underline} />
                    </Styles.ButtonGroup>
                    <Styles.ButtonGroup>
                        <Styles.Button className="ql-list" value="bullet" icon={Icon.BulletList} />
                        <Styles.Button
                            className="ql-list"
                            value="ordered"
                            icon={Icon.OrderedList}
                        />
                    </Styles.ButtonGroup>
                </Styles.ButtonGroup>
                <Styles.ButtonGroup>
                    {/* Autosave, Action buttons, Overflow (...) menu */}
                    {props.children}
                </Styles.ButtonGroup>
            </Styles.GridToolbar>
            <Styles.SideToolbar></Styles.SideToolbar>
        </Styles.Toolbar>
    );
}
