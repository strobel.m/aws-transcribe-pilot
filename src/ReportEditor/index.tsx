import React from 'react';
import { ThemeProvider } from 'styled-components';

import { RichText } from '@smartreporting/rich-text';
import { TemplateInterface } from '@smartreporting/template';
import { ReportInterface } from '@smartreporting/report';
import {
    EditorWrapper,
    QuillEditor,
    QuillProxyInterface,
    QuillRange,
    QuillWrapper,
    Sidebar,
    StructureEditingActions,
    useEngine,
    // useNuanceSpeechRecognition,
    useStructureEditingReducer,
    useAutosaveEffect,
    // AutosaveClientLibInterface,
} from '@smartreporting/report-editor';

import { QuillToolbar } from './QuillToolbar';

import { lightTheme } from '../Themes/light';

import { S3Saver } from '../AwsTranscribe/Streaming';

// import { NuanceDevLicense } from '../Nuance';

export function ReportEditor(props: {
    readonly report: ReportInterface;
    readonly template: TemplateInterface;
    readonly reportId: number;
}) {
    const [state, dispatch] = useStructureEditingReducer(props.report, props.template);
    const quillProxyRef = React.useRef<QuillProxyInterface>();
    const toolbarRef = React.createRef<HTMLDivElement>();
    const storeQuillState = (document: RichText, range?: QuillRange) =>
        dispatch(StructureEditingActions.updateQuillState(document, range));

    useEngine(state, dispatch, ENGINE_DEBOUNCE_DELAY_IN_MILLISECONDS);

    function onSaveReportFailure(e: Error) {
        console.error('error while saving report ' + e.message);
    }

    // workaround
    // const { serverUrl } = NuanceDevLicense;
    // setCookie('NUSA_ServerURL', serverUrl);
    // useNuanceSpeechRecognition(quillProxyRef.current?.getEditorDomNode(), NuanceDevLicense, alert);

    useAutosaveEffect(
        state.report,
        state.quillDocument,
        new S3Saver(props.reportId),
        onSaveReportFailure
    );

    // useNuanceSpeechRecognition(quillProxyRef.current?.getEditorDomNode(), NuanceDevLicense, alert);

    return (
        <ThemeProvider theme={lightTheme}>
            <EditorWrapper>
                <Sidebar
                    report={state.report}
                    template={state.template}
                    dispatch={dispatch}
                    focusedPath={state.sidebarNavigation.focusedPath}
                    lowestExpandedPath={state.sidebarNavigation.lowestExpandedPath}
                />
                <QuillWrapper>
                    <QuillToolbar toolbarRef={toolbarRef}></QuillToolbar>
                    <QuillEditor
                        toolbarRef={toolbarRef}
                        document={state.quillDocument}
                        range={state.quillRange}
                        onDocumentChange={storeQuillState}
                        onRangeChange={storeQuillState}
                        quillRef={quillProxyRef}
                        focusedPath={state.sidebarNavigation.focusedPath}
                    />
                </QuillWrapper>
            </EditorWrapper>
        </ThemeProvider>
    );
}

const ENGINE_DEBOUNCE_DELAY_IN_MILLISECONDS = 1000;

// function setCookie(key: string, value: string): void {
//     document.cookie = `${encodeURIComponent(key)}=${encodeURIComponent(value)}`;
// }
