import React from 'react';
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

export const enum availableTemplates {
    pirads,
    rectalCancer,
    coreBiopsy,
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: '100%',
            maxWidth: '30%',
            backgroundColor: theme.palette.background.paper,
            textAlign: 'center',
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    })
);

type TemplateListProps = { onClick: (arg0: availableTemplates) => void };
export default function TemplateList({ onClick }: TemplateListProps) {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <h1> SmartReporting - AWS Transcribe Pilot</h1>
            <h2> Template List </h2>
            <List component="nav" aria-label="Template List">
                <ListItem button>
                    <ListItemText
                        primary="AWS | MRI | Prostate | PI-RADS 2.1™"
                        onClick={() => {
                            onClick(availableTemplates.pirads);
                        }}
                    />
                </ListItem>
                <ListItem button>
                    <ListItemText
                        primary="AWS | MRI | Rectal Cancer Primary Staging"
                        onClick={() => {
                            onClick(availableTemplates.rectalCancer);
                        }}
                    />
                </ListItem>
                <ListItem button>
                    <ListItemText
                        primary="AWS | Prostate Core Biopsy"
                        onClick={() => {
                            onClick(availableTemplates.coreBiopsy);
                        }}
                    />
                </ListItem>
            </List>
        </div>
    );
}
