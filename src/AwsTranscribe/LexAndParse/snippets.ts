interface snippets {
    ProstateStandardProcedure: string;
    ProstateClinicalInformation: string;
    ProstateSize: string;
    ProstateNormalFindings: string;
    ProstateBph: string;
    ProstateBphWithProstatitis: string;
    ProstateFindingsSection: string;
    RectalCancerNormalFindings: string;
    RectalCancerFindingsSection: string;
    RectalCancerStandardProcedure: string;
    CoreBiopsyClinicalInformation: string;
    CoreBipsySpecimin: string;
    CoreBipsyTumor: string;
}

const ProstateStandardProcedure = `Procedure:
Field strength: 3 Tesla.
Endorectal coil: Acquisition with endorectal coil.
Sequences: T2w, DWI, T1w native, T1w VIBE native (3D) and T1w DCE.
DWI parameters: low b-value 50 s/mm², average b-value 800 s/mm², high b-value 1400 s/mm². ADC maps were calculated.
Contrast agent and medication: gadobutrol (dosage: 10 ml). No history of adverse contrast reactions.
Assessability: Good.
`;

const ProstateClinicalInformation = `Clinical Information:
Control date of current PSA:
Current PSA value of ng/ml.
Control date of prior PSA:
Prior PSA value of ng/ml.
Dynamics:
Biopsy date:
Histopathological results:
`;

const ProstateSize = `Anteroposterior diameter of  cm. Mediolateral diameter of  cm. Craniocaudal diameter of  cm. Volume of  ml. PSA density of  ng/ml².
`;

const ProstateNormalFindings = `Findings:
Prostate morphology: No BPH.
Peripheral zone: No suspicious lesion within the peripheral zone.
Transition zone: No suspicious lesion within the transition zone.
Lymph nodes: No suspicious lymph nodes.
Bones: No suspicious bone lesions.

Impression:
No suspicious lesion within the peripheral zone.
No suspicious lesion within the transition zone.
No suspicious lymph nodes.
No suspicious bone lesions.
`;

const ProstateBph = `Findings:
Prostate morphology: Moderate BPH. Mild bladder elevation.
Peripheral zone: No suspicious lesion within the peripheral zone.
Transition zone: No suspicious lesion within the transition zone.
Lymph nodes: No suspicious lymph nodes.
Bones: No suspicious bone lesions.

Impression:
No suspicious lesion within the peripheral zone.
No suspicious lesion within the transition zone.
No suspicious lymph nodes.
No suspicious bone lesions.
Moderate BPH.
`;

const ProstateBphWithProstatitis = `Findings:
Prostate morphology: Moderate BPH. Signs of chronic prostatitis.
Peripheral zone: No suspicious lesion within the peripheral zone.
Transition zone: No suspicious lesion within the transition zone.
Lymph nodes: No suspicious lymph nodes.
Bones: No suspicious bone lesions.

Impression:
No suspicious lesion within the peripheral zone.
No suspicious lesion within the transition zone.
No suspicious lymph nodes.
No suspicious bone lesions.
Moderate BPH. Signs of chronic prostatitis.
`;

const ProstateFindingsSection = `Findings:
Prostate morphology:
Peripheral zone:
Transition zone:
Lymph nodes (N):
Bony pelvis (M):
Other findings:
`;

const RectalCancerStandardProcedure = `Procedure
Sequences: T1w unenhanced (axial), T2w unenhanced (axial, sagittal and coronal), T1w fs post-contrast (axial) and DWI (axial).
Image quality: good.
`;

const RectalCancerNormalFindings = `Findings
Rectal mass: No evidence of a rectal mass.
Mesorectal fascia: Free.
Peritoneal reflection: No evidence of infiltration.
Lymph nodes: No suspicious lymph nodes.
Mesorectal tumor deposits: No evidence of mesorectal tumor deposits.
Extramural vascular invasion: No evidence of extramural vascular invasion.
Metastases: No evidence of metastases.

Impression
No evidence of rectal tumor.
No infiltration of mesorectal fascia.
No extramural vascular invasion.
No suspicious lymph nodes.
No suspicious metastatic lesions.
TNM classification: T0 N0 M0.
`;

const RectalCancerFindingsSection = `Findings
Rectal mass:
Mesorectal fascia:
Peritoneal reflection:
Lymph nodes:
Mesorectal tumor deposits:
Extramural vascular invasion:
Metastases:
`;

const CoreBiopsyClinicalInformation = `Clinical Information 
Clinical abnormalities:  
Previous biopsy:  
Prior Gleason score:  
Prior therapy:  
Relevant medical history:
`;

const CoreBipsySpecimin = `Macroscopic 
Number of cores:  
Size range (in mm):  
Location TRUS (core biopsies):  
Location transperineal (core biopsies)
`;

const CoreBipsyTumor = `Microscopic 
Tumor:  
Location TRUS (acinar adenocarcinoma):  
Location transperineal (acinar adenocarcinoma):  
Total Gleason (Grade Group):  
Total length of cancer in core:  mm. 
Type of measurement:  
Extraprostatic extension:  
Perineural invasion:  
Lymphovascular invasion:
`;

export const Snippets: snippets = {
    ProstateStandardProcedure,
    ProstateClinicalInformation,
    ProstateSize,
    ProstateNormalFindings,
    ProstateBph,
    ProstateBphWithProstatitis,
    ProstateFindingsSection,
    RectalCancerNormalFindings,
    RectalCancerStandardProcedure,
    RectalCancerFindingsSection,
    CoreBiopsyClinicalInformation,
    CoreBipsySpecimin,
    CoreBipsyTumor,
};
