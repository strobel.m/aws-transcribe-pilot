import moo from 'moo';
import { Quill } from 'quill';
import { QuillWithHistoryInterface, LexAndParseInterface } from '../types';
import { getQuill } from '../Helpers';

import { Snippets } from './snippets';

export class LexAndParse implements LexAndParseInterface {
    private quill: QuillWithHistoryInterface;

    constructor(quill: QuillWithHistoryInterface) {
        this.quill = quill;
    }

    lexAndParse = (str: string) => {
        return lexAndParseImpl(str, this.quill);
    };
}

const lexer = moo.compile({
    // openReport: /[o|O]pen [R|r]eport?./,
    // closeReport: /[c|C]lose [R|r]eport?./,
    resetReport: /[r|R]eset?. [R|r]eport?./,

    goToTop: /[g|G]o to(?: the)? [t|T]op?./,
    goToBottom: /[g|G]o to(?: the)? [b|B]ottom?./,
    goToWords: /[g|G]o to \w+(?:\s\w+)?.?/,

    newLine: /[n|N]ew [L|l]ine?.(?:\s)?/,
    newParagraph: /[n|N]ew [P|p]aragraph?.(?:\s)?/,

    fullStop: /[f|F]ull [S|s]top.?/,
    period: /[p|P]eriod?./,
    // comma: /[c|C]omma?./,
    colon: /[c|C]olon?./,

    openBracket: /[o|O]pen [b|B]racket.?/,
    closeBracket: /[c|C]lose [b|B]racket.?/,

    deleteLine: /[d|D]elete [l|L]ine.?/,
    deleteWord: /[d|D]elete.?/,

    ProstateStandardProcedure: /[p|P]rostate [s|S]tandard [p|P]rocedure.?/,
    ProstateClinicalInformation: /[p|P]rostate [c|C]linical [i|I]nformation.?/,
    ProstateSize: /[p|P]rostate [s|S]ize.?/,
    ProstateNormalFindings: /[p|P]rostate [n|N]ormal [f|F]indings.?/,
    ProstateBph: /[p|P]rostate [f|F]indings.? BPH.?/,
    ProstateBphWithProstatitis: /[p|P]rostate [f|F]indings.? [p|P]rostatitis BPH.?/,
    ProstateFindingsSection: /[p|P]rostate [f|F]indings? [s|S]ection.?/,
    RectalCancerStandardProcedure: /[r|R]ectal [c|C]ancer [s|S]tandard [p|P]rocedure.?/,
    RectalCancerNormalFindings: /[r|R]ectal [c|C]ancer [n|N]ormal [f|F]indings.?/,
    RectalCancerFindingsSection: /[r|R]ectal [c|C]ancer [f|F]inding [s|S]ection.?/,

    CoreBiopsyClinicalInformation: /[p|P]rostate [b|B]iopsy,? [c|C]linical [i|I]nformation.?/,
    CoreBipsySpecimin: /[p|P]rostate [b|B]iopsy [s|S]pecimen.?/,
    CoreBipsyTumor: /[p|P]rostate [b|B]iopsy [t|T]umor.?/,

    undo: /[U|u]nd(?:o|ue)\.?/,
    revert: /[R|r]evert\.?/,

    redo: /[R|r]edo.?/,

    text: moo.fallback,
});

function lexText(lexer: moo.Lexer, str: string) {
    const tokens: Array<moo.Token> = Array.from(lexer.reset(str));

    return tokens;
}

function lexAndParseImpl(str: string, quill: QuillWithHistoryInterface) {
    const tokens = lexText(lexer, str);

    const text = parseTokens(tokens, quill ?? getQuill());
    return text;
}

function closeReport() {
    return;
}

function openReport() {
    return;
}

function resetReport(quill: Quill) {
    const transLenght = quill.getText().length;

    quill.deleteText(0, transLenght);
}

function goToTop(quill: Quill) {
    const range = { index: 0, length: 0 };
    quill.setSelection(range);
}
function goToBottom(quill: Quill) {
    const transLenght = quill.getText().length;

    const range = { index: transLenght, length: 0 };
    quill.setSelection(range);
}

function goToWords(quill: Quill, tok: moo.Token) {
    const searchWords = tok.text.split(' ').slice(2).join(' ');
    const searchWordsAlphaNumeric = searchWords.replace(/[^\w ]/g, '').toLowerCase();

    const sourceStr = quill.getText().toLowerCase();
    const indexes = [...sourceStr.matchAll(new RegExp(searchWordsAlphaNumeric, 'gi'))].map(
        a => a.index
    ) as Array<number>;

    const currentPosition = quill.getSelection(true).index;
    const sortedIndexes = indexes.sort(
        (a: number, b: number) => Math.abs(currentPosition - a) - Math.abs(currentPosition - b)
    );

    const [index] = sortedIndexes;

    if (index != undefined && index !== -1) {
        const wordEnd = index; //+ searchWords.length;
        const range = { index: wordEnd, length: 0 };
        quill.setSelection(range);
    }
}

function deleteLine(quill: Quill) {
    const currentPosition = quill.getSelection(true).index;
    const [, offset] = quill.getLine(currentPosition);

    const start = currentPosition - offset;
    const length = quill.getText().slice(start).indexOf('\n') + 1;

    quill.deleteText(start, length, 'api');
}

function deleteWord(quill: Quill) {
    const currentPosition = quill.getSelection(true).index;

    function getClosestWordRange(str: string, pos: number) {
        const left = str.slice(0, pos + 1).search(/\S+$/);
        const right = str.slice(pos).search(/\s/);

        if (left < 0) {
            throw new Error('Could not find left word');
        }
        if (right < 0) {
            throw new Error('Could not find right word');
        }
        return { index: left, length: pos - left + right };
    }

    const { index, length } = getClosestWordRange(quill.getText(), currentPosition);
    quill.deleteText(index, length);
}

const maxIterations = 3;
function undo(quill: QuillWithHistoryInterface) {
    const currentText = quill.getText();

    let it = maxIterations;
    while (it && quill.getText() === currentText) {
        try {
            quill.history.undo();
            it--;
        } catch (e) {
            console.error(e);
            it = 0;
        }
    }
}
function redo(quill: QuillWithHistoryInterface) {
    const currentText = quill.getText();

    let it = maxIterations;
    while (it && quill.getText() === currentText) {
        try {
            quill.history.redo();
            it--;
        } catch (e) {
            console.error(e);
            it = 0;
        }
    }
}

function parseTokens(tokens: moo.Token[], quill: QuillWithHistoryInterface): string {
    let text = '';
    for (const tok of tokens) {
        switch (tok.type) {
            case 'openReport': {
                openReport();
                break;
            }
            case 'closeReport': {
                closeReport();
                break;
            }

            case 'resetReport':
                resetReport(quill);
                break;
            case 'goToTop':
                goToTop(quill);
                break;
            case 'goToBottom':
                goToBottom(quill);
                break;
            case 'goToWords':
                goToWords(quill, tok);
                break;
            case 'newLine':
                text = text.trimRight() + '\n';
                break;
            case 'newParagraph':
                text = text.trimRight() + '\n\n';
                break;
            case 'ProstateStandardProcedure':
                text += Snippets.ProstateStandardProcedure;
                break;
            case 'ProstateClinicalInformation':
                text += Snippets.ProstateClinicalInformation;
                break;
            case 'ProstateSize':
                text += Snippets.ProstateSize;
                break;
            case 'ProstateFindingsSection':
                text += Snippets.ProstateFindingsSection;
                break;
            case 'ProstateNormalFindings':
                text += Snippets.ProstateNormalFindings;
                break;
            case 'ProstateBph':
                text += Snippets.ProstateBph;
                break;
            case 'ProstateBphWithProstatitis':
                text += Snippets.ProstateBphWithProstatitis;
                break;
            case 'RectalCancerStandardProcedure':
                text += Snippets.RectalCancerStandardProcedure;
                break;
            case 'RectalCancerNormalFindings':
                text += Snippets.RectalCancerNormalFindings;
                break;
            case 'RectalCancerFindingsSection':
                text += Snippets.RectalCancerFindingsSection;
                break;
            case 'CoreBiopsyClinicalInformation':
                text += Snippets.CoreBiopsyClinicalInformation;
                break;
            case 'CoreBipsySpecimin':
                text += Snippets.CoreBipsySpecimin;
                break;
            case 'CoreBipsyTumor':
                text += Snippets.CoreBipsyTumor;
                break;

            case 'fullStop':
            case 'period':
                text = text.trimRight() + '. ';
                break;
            case 'comma':
                text = text.trimRight() + ', ';
                break;
            case 'colon':
                text = text.trimRight() + ': ';
                break;
            case 'openBracket':
                text += '(';
                break;
            case 'closeBracket':
                text += ')';
                break;
            case 'undo':
            case 'revert':
                undo(quill);
                break;
            case 'redo':
                redo(quill);
                break;
            case 'deleteWord':
                deleteWord(quill);
                break;
            case 'deleteLine':
                deleteLine(quill);
                break;
            default:
                text += tok.text + ' ';
        }
    }
    return text;
}
