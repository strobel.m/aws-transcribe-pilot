import { QuillWithHistoryInterface } from './types';

export function getQuill(): QuillWithHistoryInterface | null {
    const [transcriptQuill] = Array.from(
        document.querySelectorAll('[data-test="sr-quill-editor"]')
    ) as any;
    const quill = transcriptQuill?.__quill as QuillWithHistoryInterface;

    return quill;
}
