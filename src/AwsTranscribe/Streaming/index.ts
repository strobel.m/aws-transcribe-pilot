// @ts-ignore
import mic from 'microphone-stream';
import crypto from 'crypto'; // tot sign our pre-signed URL
import Delta from 'quill-delta';

import { EventStreamMarshaller } from '@aws-sdk/eventstream-marshaller';
import { toUtf8, fromUtf8 } from '@aws-sdk/util-utf8-node';

import { downsampleBuffer, pcmEncode } from '../AudioUtils';
import { createPresignedURL } from '../AwsSignatureV4';
import AwsCredentials from '../AwsCredentials';
import { getQuill } from '../Helpers';
import { LexAndParse } from '../LexAndParse';
import AWS from 'aws-sdk';
import { Auth } from 'aws-amplify';

import { AutosaveClientLibInterface } from '@smartreporting/report-editor';
import { ReportInterface } from '@smartreporting/report';

// AWS
const { accessKeyId, secretAccessKey, sessionToken } = AwsCredentials;
const region = 'eu-central-1';
AWS.config.update({
    region,
    credentials: new AWS.Credentials(accessKeyId, secretAccessKey, sessionToken),
});
const s3 = new AWS.S3();

// our converter between binary event streams messages and JSON
const eventStreamMarshaller = new EventStreamMarshaller(toUtf8, fromUtf8);

// our global variables for managing state
const languageCode = 'en-US';
const sampleRate = 44100;
let inputSampleRate = 44100;
// @ts-ignore
let transcription = '';
let socket: any;
let micStream: any;
let socketError = false;
let transcribeException = false;

let currentReportId = -1;
let audioUploadCounter = 0;
let transcriptionUploadCounter = 0;
let reportUploadCounter = 0;
let uploadPath = 'unknown';

let previousPartialInsertionRange: { index: number; length: number } | undefined;
let lastPartialUpdate = Date.now();
export function streamAudioToWebSocketWithReportID(ReportID: number) {
    if (ReportID == null || ReportID === 0 || ReportID == -1) {
        throw new Error('No proper ReportID');
    }
    return async (userMediaStream: any) => {
        if (ReportID !== currentReportId) {
            currentReportId = ReportID;
            audioUploadCounter = 0;
            reportUploadCounter = 0;
            transcriptionUploadCounter = 0;
            const tokens = await Auth.currentSession();
            const userName = tokens.getIdToken().payload['cognito:username'];

            uploadPath = `${ReportID}_${userName ?? 'unknown'}`;
        }
        micStream = new mic();

        micStream.on('format', (data: any) => {
            inputSampleRate = data.sampleRate;
        });

        micStream.setStream(userMediaStream);

        // Recoding to be saved in AWS
        const recordedChunks: any = [];
        const options = { mimeType: 'audio/webm; codecs=opus' };
        const mediaRecorder = new MediaRecorder(userMediaStream, options);
        mediaRecorder.ondataavailable = handleDataAvailable;
        mediaRecorder.start();
        function handleDataAvailable(event: any) {
            if (event.data.size > 0) {
                recordedChunks.push(event.data);
                const blob = new Blob(recordedChunks, {
                    type: 'audio/webm',
                });
                const params = {
                    Body: blob,
                    Bucket: 'aws-transcribe-pilot/transcribe-uploads',
                    Key: `${uploadPath}/audioChunk${padToFive(audioUploadCounter++)}.webm`,
                };
                s3.putObject(params, function (err, data) {
                    if (err) console.error(err, err.stack);
                    else console.error(data);
                });
            }
        }

        // Transcribe Streaming
        const url = createPresignedUrl();

        socket = new WebSocket(url);
        socket.binaryType = 'arraybuffer';

        socket.onopen = () => {
            micStream.on('data', (rawAudioChunk: any) => {
                const binary = convertAudioToBinaryMessage(rawAudioChunk);
                if (socket.readyState === socket.OPEN) socket.send(binary);
            });
        };

        wireSocketEvents();
    };

    function wireSocketEvents() {
        socket.onmessage = ({ data }: any) => {
            const messageWrapper = eventStreamMarshaller.unmarshall(Buffer.from(data));
            const messageBody = JSON.parse(
                // @ts-ignore
                String.fromCharCode.apply(String, messageWrapper.body)
            );
            if (messageWrapper.headers[':message-type'].value === 'event') {
                handleEventStreamMessage(messageBody);
            } else {
                transcribeException = true;
                console.error(messageBody.Message);
                showError(messageBody.Message);
            }
        };

        socket.onerror = () => {
            socketError = true;
            showError('WebSocket connection error. Try again.');
        };

        socket.onclose = ({ code, reason }: any) => {
            micStream.stop();

            if (!socketError && !transcribeException) {
                if (code != 1000) {
                    showError(`Streaming Exception: ${reason}`);
                }
            }
        };
    }

    async function handleEventStreamMessage(messageBody: any) {
        const results = messageBody.Transcript.Results;

        if (results.length > 0) {
            if (results[0].Alternatives.length > 0) {
                let transcript = results[0].Alternatives[0].Transcript;

                transcript = decodeURIComponent(escape(transcript));

                const contentIsPartial = results[0].IsPartial;

                const quill = getQuill();
                if (quill == undefined) return;

                const pos = quill.getSelection(true);
                if (pos == undefined) return;

                const lexAndParse = new LexAndParse(quill).lexAndParse;
                const parsedText = lexAndParse(transcript);

                const { index, length } = previousPartialInsertionRange ?? quill.getSelection(true);
                if (contentIsPartial) {
                    if (Date.now() - lastPartialUpdate > 250) {
                        lastPartialUpdate = Date.now();
                        quill.updateContents(
                            new Delta().retain(index).delete(length).insert(parsedText)
                        );
                        previousPartialInsertionRange = { index, length: parsedText.length };
                    }
                } else {
                    quill.updateContents(new Delta().retain(index).delete(length));
                    quill.insertText(quill.getSelection(true).index, parsedText);
                    previousPartialInsertionRange = undefined;
                    transcription += transcript;

                    const partialReportParameters = {
                        Body: transcript,
                        Bucket: 'aws-transcribe-pilot/transcribe-uploads',
                        Key: `${uploadPath}/transcription${padToFive(
                            transcriptionUploadCounter++
                        )}.txt`,
                    };
                    s3.putObject(partialReportParameters, function (err, data) {
                        if (err) console.error(err, err.stack);
                        else console.error(data);
                    });
                    // S3 Upload whole report
                    const wholeReportParameters = {
                        Body: transcription,
                        Bucket: 'aws-transcribe-pilot/transcribe-uploads',
                        Key: `${uploadPath}/transcription.txt`,
                    };
                    s3.putObject(wholeReportParameters, function (err, data) {
                        if (err) console.error(err, err.stack);
                        else console.error(data);
                    });
                }
            }
        }
    }
}

export const closeSocket = () => {
    if (socket != null && socket.readyState === socket.OPEN) {
        micStream.stop();
        const emptyMessage = getAudioEventMessage(Buffer.from(new Buffer([])));
        // @ts-ignore
        const emptyBuffer = eventStreamMarshaller.marshall(emptyMessage);
        socket.send(emptyBuffer);
    }
};

function convertAudioToBinaryMessage(audioChunk: any) {
    const raw = mic.toRaw(audioChunk);

    if (raw == null) return;
    const downsampledBuffer = downsampleBuffer(raw, inputSampleRate, sampleRate);
    const pcmEncodedBuffer = pcmEncode(downsampledBuffer);

    const audioEventMessage = getAudioEventMessage(Buffer.from(pcmEncodedBuffer));

    // @ts-ignore
    const binary = eventStreamMarshaller.marshall(audioEventMessage);

    return binary;
}

function getAudioEventMessage(buffer: any) {
    return {
        headers: {
            ':message-type': {
                type: 'string',
                value: 'event',
            },
            ':event-type': {
                type: 'string',
                value: 'AudioEvent',
            },
        },
        body: buffer,
    };
}

function createPresignedUrl() {
    const endpoint = `transcribestreaming.${region}.amazonaws.com:8443`;
    return createPresignedURL(
        'GET',
        endpoint,
        '/medical-stream-transcription-websocket',
        'transcribe',
        crypto.createHash('sha256').update('', 'utf8').digest('hex'),
        {
            key: accessKeyId,
            secret: secretAccessKey,
            sessionToken: sessionToken,
            protocol: 'wss',
            expires: 15,
            region,
            query: `language-code=${languageCode}&media-encoding=pcm&sample-rate=${sampleRate}&specialty=RADIOLOGY&type=DICTATION`,
        }
    );
}

function showError(msg: string) {
    window.alert(msg);
}

const padToFive = (arg: number) => (arg <= 99999 ? `0000${arg}`.slice(-5) : arg);

export class S3Saver implements AutosaveClientLibInterface {
    constructor(ReportID: number) {
        if (ReportID == 0 || ReportID === -1) {
            throw new Error('No proper ReportID');
        }
    }
    async saveReport(report: ReportInterface): Promise<void> {
        const reportAndMetadata = { uploadTime: Date.now(), report: report.toJsonObject() };
        const params = {
            Body: JSON.stringify(reportAndMetadata),
            Bucket: 'aws-transcribe-pilot/transcribe-uploads',
            Key: `${uploadPath}/report${padToFive(reportUploadCounter++)}.json`,
        };
        s3.putObject(params, function (err, data) {
            if (err) console.error(err, err.stack);
            else console.error(data);
        });
    }
}
