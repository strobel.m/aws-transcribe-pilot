import { Quill } from 'quill';
export interface VoiceIntegrationInterface {
    start: () => void;
    stop: () => void;
}

export interface LexAndParseInterface {
    lexAndParse: (str: string) => string;
}

export interface QuillWithHistoryInterface extends Quill {
    history: {
        undo: () => void;
        redo: () => void;
    };
}
