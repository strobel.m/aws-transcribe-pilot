import { VoiceIntegrationInterface } from './types';

import { streamAudioToWebSocketWithReportID, closeSocket } from './Streaming';

export class Transcribe implements VoiceIntegrationInterface {
    static hasReceivedFinalMessage = true;
    private static isRunning = false;

    private readonly ReportID: number;
    constructor(ReportID: number) {
        if (ReportID == null) {
            throw new Error('The Report Id cannot be empty');
        }
        this.ReportID = ReportID;
    }

    public async start() {
        if (Transcribe.isRunning) {
            return;
        }

        Transcribe.isRunning = true;

        return window.navigator.mediaDevices
            .getUserMedia({
                video: false,
                audio: true,
            })
            .then(arg => streamAudioToWebSocketWithReportID(this.ReportID)(arg));
    }

    public stop() {
        setTimeout(() => {
            Transcribe.isRunning = false;
            closeSocket();
        }, 1000);
    }
}
