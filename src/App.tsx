import CloseIcon from '@material-ui/icons/Close';

import Amplify from 'aws-amplify';
import { withAuthenticator, AmplifySignOut } from '@aws-amplify/ui-react';

import '../static/css/fonts.css';
import '../static/css/normalize.css';

import { Report, ReportInterface } from '@smartreporting/report';
import { Locale } from '@smartreporting/report-editor';

import React from 'react';

import { UnreachableCaseError } from 'ts-essentials';

// import { TemplateConverter } from '@smartreporting/template-converter';
import { TemplateInterface, Template } from '@smartreporting/template';
import { ReportEditor } from './ReportEditor';

import { Transcribe } from './AwsTranscribe';
import TranscribeWrapper from './TranscribeWrapper';

import TemplateList, { availableTemplates } from './TemplateList';

import pirads from './Templates/V1Templates/AWS_PI-RADS_2.1.json';
import rectalCancer from './Templates/V1Templates/AWS_MRI_Rectal_Cancer.json';
import coreBiopsy from './Templates/V1Templates/AWS_Core_Biopsy.json';

import awsExports from './aws-exports';

Amplify.configure(awsExports);

type AppState = {
    reportId?: number;
    report?: ReportInterface;
    template?: TemplateInterface;
    transcribe?: Transcribe;
};

function App() {
    const [state, setState] = React.useState<AppState>(generateNewAppState(undefined));

    const { reportId, report, template, transcribe } = state;

    function onCloseButtonClick() {
        setState({});
    }

    async function onTemplateClick(item: availableTemplates): Promise<void> {
        const result = getTemplate(item);
        setState(generateNewAppState(await result));
    }

    if (reportId == undefined || !template || !report || !transcribe) {
        return (
            <>
                <AmplifySignOut />
                <TemplateList onClick={onTemplateClick} />
            </>
        );
    }

    return (
        <>
            <AmplifySignOut />
            <div style={{ display: 'flex' }}>
                <CloseIcon style={{ marginLeft: 'auto' }} onClick={onCloseButtonClick} />
            </div>
            <ReportEditor report={report} template={template} reportId={reportId} />
            <TranscribeWrapper transcribe={transcribe} />
        </>
    );
}

async function getTemplate(item: availableTemplates): Promise<TemplateInterface> {
    switch (item) {
        case availableTemplates.pirads: {
            // @ts-ignore
            return Template.fromJsonObject(pirads);
        }
        case availableTemplates.rectalCancer: {
            // @ts-ignore
            return Template.fromJsonObject(rectalCancer);
            // const { template } = await TemplateConverter.fromLegacyTemplateData(rectalCancer);
            // return template;
        }
        case availableTemplates.coreBiopsy: {
            // @ts-ignore
            return Template.fromJsonObject(coreBiopsy);
            // const { template } = await TemplateConverter.fromLegacyTemplateData(rectalCancer);
            // return template;
        }
        default:
            throw new UnreachableCaseError(item);
    }
}

function generateNewAppState(template: TemplateInterface | undefined): AppState {
    const id = Date.now();
    return {
        reportId: id,
        report: new Report('', Locale.AmericanEnglish, { id, version: -1 }),
        template,
        transcribe: new Transcribe(id),
    };
}

export default withAuthenticator(App);
