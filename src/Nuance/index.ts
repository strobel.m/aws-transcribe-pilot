import { NuanceConfig } from '@smartreporting/report-editor';

export const NuanceDevLicense: NuanceConfig = {
    organizationToken: '09cd189e-97ee-4392-8c3e-b9851a1a5170',
    partnerGuid: '89b31887-3b03-429a-9644-a8d3d33a8885',
    applicationName: 'Smart Radiology',
    userId: 'development-test-user',
    scriptUrl: 'https://speechanywhere.nuancehdp.com/mainline/scripts/Nuance.SpeechAnywhere.js',
};
